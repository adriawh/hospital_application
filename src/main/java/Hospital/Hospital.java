package Hospital;

import java.util.ArrayList;

/**
 * Class to store hospitals, each hospital contains a list of its departments
 */
public class Hospital {
    final private String hospitalName;
    private ArrayList<Department> departments;

    public Hospital(String hospitalName) {
        this.hospitalName = hospitalName;
        departments = new ArrayList<>();
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public ArrayList<Department> getDepartments() {
        return departments;
    }

    public void addDepartment(Department department){
        departments.add(department);
    }

    @Override
    public String toString() {
        return "Hospital.Hospital{" +
                "hospitalName='" + hospitalName + '\'' +
                ", departments=" + departments +
                '}';
    }
}
