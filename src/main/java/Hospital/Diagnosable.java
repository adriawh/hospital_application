package Hospital;

/**
 * Says if an object of type Person is able to be diagnosed
 */
public interface Diagnosable {

    void setDiagnosis(String diagnosis);

}
