package Hospital;

import Hospital.exception.RemoveException;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Class to store department objects which contains registers over employees and patients
 */

public class Department {
    private String departmentName;
    private ArrayList<Patient> patients;
    private ArrayList<Employee> employees;

    public Department(String departmentName) {
        this.departmentName = departmentName;
        patients = new ArrayList<>();
        employees = new ArrayList<>();
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public ArrayList<Employee> getEmployees() {
        return employees;
    }

    public void addEmployee(Employee employee){
        if(!employees.contains(employee)){
            employees.add(employee);
        }
    }

    public ArrayList<Patient> getPatients() {
        return patients;
    }

    public void addPatient(Patient patient){
       if(!patients.contains(patient)){
           patients.add(patient);
       }
    }

    /**
     * This method removes a person of either type patient or employee from the departments register.
     *
     * @param person person-object to be removed from the department
     * @throws RemoveException if the person does not exist in the register
     * @throws NullPointerException if the object given is null
     */
    public void remove(Person person) throws RemoveException, NullPointerException {
        if(person == null){
            throw new NullPointerException("NULL cannot be removed from register");
        }else if(patients.contains(person)){
            patients.remove(person);
        }else if(employees.contains(person)){
            employees.remove(person);
        }else{
            throw new RemoveException("Could not remove person with name: '" + person.getFullName() +
                    "' from '" + departmentName +  "' since \n said person does not exist in the register");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return Objects.equals(departmentName, that.departmentName) &&
                Objects.equals(patients, that.patients) &&
                Objects.equals(employees, that.employees);
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName, patients, employees);
    }

    @Override
    public String toString() {
        return "Hospital.Department{" +
                "departmentName='" + departmentName + '\'' +
                ", patients=" + patients +
                ", employees=" + employees +
                '}';
    }
}
