package Hospital;

/**
 * Class to represent a patient, a patient can receive a diagnosis
 */
public class Patient extends Person implements Diagnosable {
    private String diagnosis = "";

    protected Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    protected String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    @Override
    public String toString() {
        return "Patient{" + super.toString() +
                "diagnosis='" + diagnosis + '\'' +
                '}';
    }
}


