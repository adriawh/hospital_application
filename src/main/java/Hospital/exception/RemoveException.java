package Hospital.exception;

/**
 * Exceptionclass made for remove() in Department.java.
 * Is to be used when remove() is unable to remove an object of type Person
 */
public class RemoveException extends Throwable {
    private static final long serialVersionUID = 1L;

    /**
     *
     * @param error the errormessage the developer wants to display when thrown
     */
    public RemoveException(String error) {
        super(error);
    }
}
