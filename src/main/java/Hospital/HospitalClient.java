package Hospital;

import Hospital.exception.RemoveException;
import Hospital.healthpersonal.Nurse;
import Hospital.healthpersonal.doctor.GeneralPractitioner;
import Hospital.healthpersonal.doctor.Surgeon;

public class HospitalClient {

    public static void main(String[] args){
        Hospital hospital = new Hospital("St.Olavs");

        /**
         * Fills the hospital object with data given in the assignment explanation
         */
        fillRegisterWithTestData(hospital);

        for(Department d : hospital.getDepartments()){
            /**
             * Calls the remove() method to remove an existing employee
             */
            if(d.getDepartmentName().equals("Akutten")){
                try{
                    d.remove(new Employee("Odd Even", "Primtallet", ""));
                    System.out.println("Person fjernet fra register\n");
                }catch(RemoveException | NullPointerException e){
                    System.out.println(e);
                }
            }
            /**
             * Calls the remove() method to remove a non-existing patient
             */
            if(d.getDepartmentName().equals("Barn poliklinikk")){
                try{
                    d.remove(new Patient("Jøra", "Drebli", ""));
                }catch(RemoveException | NullPointerException e){
                    System.out.println(e);
                }
            }
        }
    }

    /**
     * Method given in the assignment explanation
     * @param hospital the hospital object the dtat is to be added
     */
    public static void fillRegisterWithTestData(final Hospital hospital) {

        // Add some departments
        Department emergency = new Department("Akutten");
        emergency.getEmployees().add(new Employee("Odd Even", "Primtallet", ""));
        emergency.getEmployees().add(new Employee("Huppasahn", "DelFinito", ""));
        emergency.getEmployees().add(new Employee("Rigmor", "Mortis", ""));
        emergency.getEmployees().add(new GeneralPractitioner("Inco", "Gnito", ""));
        emergency.getEmployees().add(new Surgeon("Inco", "Gnito", ""));
        emergency.getEmployees().add(new Nurse("Nina", "Teknologi", ""));
        emergency.getEmployees().add(new Nurse("Ove", "Ralt", ""));
        emergency.getPatients().add(new Patient("Inga", "Lykke", ""));
        emergency.getPatients().add(new Patient("Ulrik", "Smål", ""));
        hospital.addDepartment(emergency);

        Department childrenPolyclinic = new Department("Barn poliklinikk");
        childrenPolyclinic.getEmployees().add(new Employee("Salti", "Kaffen", ""));
        childrenPolyclinic.getEmployees().add(new Employee("Nidel V.", "Elvefølger", ""));
        childrenPolyclinic.getEmployees().add(new Employee("Anton", "Nym", ""));
        childrenPolyclinic.getEmployees().add(new GeneralPractitioner("Gene", "Sis", ""));
        childrenPolyclinic.getEmployees().add(new Surgeon("Nanna", "Na", ""));
        childrenPolyclinic.getEmployees().add(new Nurse("Nora", "Toriet", ""));
        childrenPolyclinic.getPatients().add(new Patient("Hans", "Omvar", ""));
        childrenPolyclinic.getPatients().add(new Patient("Laila", "La", ""));
        childrenPolyclinic.getPatients().add(new Patient("Jøran", "Drebli", ""));
        hospital.addDepartment(childrenPolyclinic);
    }

}
