package Hospital.healthpersonal.doctor;

import Hospital.Employee;
import Hospital.Patient;

/**
 * Abstract class to store doctor employees
 */
public abstract class Doctor extends Employee {
    protected Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    public abstract void setDiagnosis(Patient patient, String diagnosis);

}
