package Hospital;

import Hospital.exception.RemoveException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for remove() in Department.java.
 */
public class DepartmentTest {

    @Nested
    @DisplayName("Remove succesfull when:")
    class removeSuccessful {

        /**
         * Tests the remove() method when you remove an existing employee in the register.
         */
        @Test
        @DisplayName("Existing employee is given")
        public void removeSuccessfulWhenCorrectDataGivenEmployee() {
            Department department = new Department("test");
            Employee testPerson = new Employee("Test", "testesen", "1234");

            department.addEmployee(testPerson);
            Assertions.assertTrue(department.getEmployees().contains(testPerson));

            try {
                department.remove(testPerson);
            } catch (RemoveException | NullPointerException e) {
                assertNull(e);
            }
            assertFalse(department.getEmployees().contains(testPerson));
        }

        /**
         * Tests the remove() method when you remove an existing patient in the register.
         */
        @Test
        @DisplayName("Existing patient is given")
        public void removeSuccessfulWhenCorrectDataGivenPatient() {
            Department department = new Department("test");
            Patient testPerson = new Patient("Test", "testesen", "1234");

            department.addPatient(testPerson);
            Assertions.assertTrue(department.getPatients().contains(testPerson));

            try {
                department.remove(testPerson);
            } catch (RemoveException | NullPointerException e) {
                assertNull(e);
            }
            assertFalse(department.getEmployees().contains(testPerson));
        }
    }

    @Nested
    @DisplayName("Method handles unsuccessful removes correctly when:")
    class removeUnsuccessful {

        /**
         * Tests that the remove() method responds with the correct
         * message when a null-input is given.
         */
        @Test
        @DisplayName("Null input is given, prints correct message")
        public void nullInput(){
            Department department = new Department("test");

            try{
                department.remove(null);
            }catch(RemoveException | NullPointerException e){
                assertEquals(e.getMessage(), "NULL cannot be removed from register");
            }
        }

        /**
         * Tests that the remove() method responds with the correct
         * message when the given person to remove doesnt exist in the register.
         */
        @Test
        @DisplayName("Non existing object is given, prints correct message")
        public void correctExceptionMessage(){
            Department department = new Department("test");
            Employee testPerson2 = new Employee("Testan", "testesensene", "1235");

            try {
                department.remove(testPerson2);
            } catch (RemoveException | NullPointerException e) {
                assertEquals(e.getMessage(), "Could not remove person with name: '" + testPerson2.getFullName() +
                        "' from '" + department.getDepartmentName() + "' since \n said person does not exist in the register");
            }
        }
    }
}